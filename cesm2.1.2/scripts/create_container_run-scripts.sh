#!/bin/bash
#
# Creates all necessary files to run CESM 2.1.2 in CentOS7 containers
# Copy this file into your scripts folder and make it executable
# chmod +x create_container_run-scripts.sh
#
# Urs Beyerle, IAC/ETH
#

CASE=$1
echo
if [ ! $CASE ]; then
    echo "Usage:  ./create_container_run-scripts.sh <CASE>"
    echo; exit
fi

cd $CASE || exit 1

# create container.run
echo "#!/bin/bash"       > container.run
grep ^#SBATCH .case.run >> container.run
echo                    >> container.run
echo "run-centos7 /bin/bash -i -l <<EOF"                  >> container.run
echo "unset SINGULARITY_BIND"                             >> container.run
echo "cd /cluster/home/$USER/cesm212/cime/scripts/$CASE"  >> container.run
echo "./.case.run"                                        >> container.run
echo "EOF"                                                >> container.run
echo "$PWD/container.run created"

# create container-st_archive.run
echo "#!/bin/bash"              > container-st_archive.run
grep ^#SBATCH case.st_archive  >> container-st_archive.run
echo                           >> container-st_archive.run
echo "run-centos7 /bin/bash -i -l <<EOF"                  >> container-st_archive.run
echo "cd /cluster/home/$USER/cesm212/cime/scripts/$CASE"  >> container-st_archive.run
echo "./case.st_archive"                                  >> container-st_archive.run
echo "EOF"                                                >> container-st_archive.run
echo "$PWD/container-st_archive.run created"

# create container.submit
cat << 'EOF' > container.submit
#!/bin/bash
job_id=$( sbatch < container.run | awk '{print $4}' )
archive_id=$( sbatch --dependency=afterok:$job_id container-st_archive.run | awk '{print $4}' )
echo "Submitted batch jobs $job_id and $archive_id"
EOF
chmod +x container.submit
echo "$PWD/container.submit created"

# create container.resubmit
cat << 'EOF' > container.resubmit
#!/bin/bash
output=$(./xmlquery RESUBMIT)
number=$(echo "$output" | grep -oP '(?<=RESUBMIT: )\d+')

if [ -z "$number" ]; then
    echo "Error: Failed to retrieve a valid RESUBMIT number."
    exit 1
fi

job_id=$(sbatch < container.run | awk '{print $4}')
archive_id=$(sbatch --dependency=afterok:$job_id container-st_archive.run | awk '{print $4}')
echo "Submitted batch jobs $job_id and $archive_id"

while [ "$number" -gt 0 ]; do
    echo "Jobs left to submit: $number"
    number=$((number - 1))
    ./xmlchange RESUBMIT=${number}

    job_id=$(sbatch --dependency=afterok:$archive_id container.run | awk '{print $4}')
    archive_id=$(sbatch --dependency=afterok:$job_id container-st_archive.run | awk '{print $4}')

    echo "Submitted batch jobs $job_id and $archive_id"
done

echo "All jobs submitted!"
EOF
chmod +x container.resubmit
echo "$PWD/container.resubmit created"

# patch env_mach_specific.xml
sed -i "s|${EXEROOT}/cesm.exe|${EXEROOT}/run_cesm.sh|" env_mach_specific.xml
sed -i "s|<executable>mpirun</executable>|<executable>mpirun run-centos7</executable>|" env_mach_specific.xml
grep -q I_MPI_SHM_LMT env_mach_specific.xml
if [ $? != 0 ]; then
    sed -i '/<env name="I_MPI_PMI_LIBRARY">unset<\/env>/a\ \ \ \ <env name="I_MPI_SHM_LMT">shm</env>' env_mach_specific.xml
fi
echo "$PWD/env_mach_specific.xml modified"

# create run_cesm.sh
cd /cluster/work/climate/$USER/$CASE/bld || exit 1
echo "#!/bin/bash"                                                           > run_cesm.sh
echo ". /cluster/work/climate/cesm/container/environment_intel-impi-2018.1" >> run_cesm.sh
echo "/cluster/work/climate/$USER/$CASE/bld/cesm.exe"                       >> run_cesm.sh
chmod +x run_cesm.sh
echo "$PWD/run_cesm.sh created"
echo
