# CESM2 configuration files for ETH cluster EULER

## How to install

* Go to your cime/config/cesm folder, for example
```
$ cd ~/cesm212/cime/config/cesm
```

* Replace config_inputdata.xml
```
$ wget https://git.iac.ethz.ch/cesm/config/-/raw/main/cesm2.1.2/config_inputdata.xml -O config_inputdata.xml
```

* Replace config_batch.xml, config_machines.xml and config_compilers.xml in machines folder
```
cd machines
wget https://git.iac.ethz.ch/cesm/config/-/raw/main/cesm2.1.2/machines/config_batch.xml     -O config_batch.xml
wget https://git.iac.ethz.ch/cesm/config/-/raw/main/cesm2.1.2/machines/config_machines.xml  -O config_machines.xml
wget https://git.iac.ethz.ch/cesm/config/-/raw/main/cesm2.1.2/machines/config_compilers.xml -O config_compilers.xml
```

## Supported EULER configurations

* Tested setups for ./create_newcase
```
./create_newcase --compiler $COMPILER --mpilib $MPILIB --mach $MACH --queue $QUEUE ....

COMPILER=intel
MPILIB=impi
MACH=euler6|euler7|euler7p1|euler7p2      # whereas euler7=euler7p1
QUEUE=normal.24h|normal.4h
```

## How to set up a CESM2 control run on ETH cluster EULER

* Run a CMIP6 piControl simulation (B1850cmip6) using 1° spatial resolution (f09_g17) on one euler7 node (128 cores) for 10 days
```
./create_newcase --case my_case --res f09_g17 --compset B1850cmip6 --mach euler7p1 --compiler intel --mpilib impi --queue normal.24h
cd my_case
./xmlchange NTASKS_CPL=128,NTASKS_ATM=128,NTASKS_OCN=128,NTASKS_ICE=128,NTASKS_LND=128,NTASKS_WAV=4,NTASKS_GLC=128,NTASKS_ROF=128,NTASKS_ESP=1
./xmlchange ROOTPE_CPL=0,ROOTPE_ATM=0,ROOTPE_OCN=0,ROOTPE_ICE=0,ROOTPE_LND=0,ROOTPE_WAV=0,ROOTPE_GLC=0,ROOTPE_ROF=0,ROOTPE_ESP=0
./xmlchange STOP_OPTION=ndays,STOP_N=10
./case.setup
./case.build
./check_input_data
./preview_run
./case.submit
```

* For more info see https://wiki.iac.ethz.ch/Climphys/ProjectCESM2SetupControlRun
